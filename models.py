from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Office(db.Model):
    __tablename__ = 'offices'
    id = db.Column(db.Integer, primary_key=True, )
    name = db.Column(db.String(60))
    address = db.Column(db.String(255))
    # lat = db.Column(db.String(255))
    # lon = db.Column(db.String(255))
    minister = db.Column(db.String(20))

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(40))
    password = db.Column(db.String(66))

