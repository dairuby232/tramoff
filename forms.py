from wtforms import Form
from wtforms import StringField
from wtforms import validators
from wtforms import StringField, TextAreaField, EmailField, PasswordField
from wtforms.validators import DataRequired


class CreateForm(Form):
    name = StringField("office name", [validators.length(min=4, max=25, message="Ingrese un username valido"),
                                         validators.data_required(message="El username es requerido")
                                         ])
    address = StringField("address", [validators.DataRequired(message="La direccion es requerida")

                       ])
    minister = StringField("Minister", [validators.data_required(message=" el ministerio es requerido")])

class EditForm(Form):
    name = StringField("office name", [validators.length(min=4, max=25, message="Ingrese un username valido"),
                                         validators.data_required(message="El username es requerido")
                                         ])
    address = StringField("address", [validators.DataRequired(message="La direccion es requerida")

                       ])
    minister = StringField("Minister", [validators.data_required(message=" el ministerio es requerido")])


class DeleteForm(Form):
    name = StringField("office name", [validators.length(min=4, max=25, message="Ingrese un username valido"),
                                         validators.data_required(message="El username es requerido")
                                         ])
    address = StringField("address", [validators.DataRequired(message="La direccion es requerida")

                       ])
    minister = StringField("Minister", [validators.data_required(message=" el ministerio es requerido")])

class ReadForm(Form):
    name = StringField("office name", [validators.length(min=4, max=25, message="Ingrese un username valido"),
                                         validators.data_required(message="El username es requerido")
                                         ])
    address = StringField("address", [validators.DataRequired(message="La direccion es requerida")

                       ])
    minister = StringField("Minister", [validators.data_required(message=" el ministerio es requerido")])


class CreateUser(Form):
    username = StringField("user name", [validators.length(min=4, max=25, message="Ingrese un username valido"),
                                         validators.data_required(message="El username es requerido")
                                         ])
    email = EmailField(" correo electronico",
                       [
                           validators.DataRequired(message="El email es requerido")
                           #    validators.Email(message="Ingrese un email valido")
                       ])
    password = PasswordField("Pasword", [validators.data_required(message=" el password es requerido")])