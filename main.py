# -*- coding: utf-8 -*-
from flask import Flask, abort
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect

from config import DevelopmentConfig
from flask import Flask, request, url_for, redirect
from flask import session
from flask import flash
from models import Office, db
from models import User
import forms

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
csrf = CSRFProtect(app)

db.init_app(app)
with app.app_context():
    db.create_all()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/create', methods=['GET', 'POST'])
def create():
    create_form = forms.CreateForm(request.form)
    if request.method == 'POST' and create_form.validate():
        office = Office(name=create_form.name.data,
                        address=create_form.address.data,
                        minister=create_form.minister.data)
        db.session.add(office)
        db.session.commit()

        success_message = 'Oficina registrada en Bd'
        flash(success_message)
    return render_template('create.html', form=create_form)


@app.route('/office')
def show_list():
    offices = Office.query.all()
    return render_template('office.html', offices=offices)

@app.route('/office/edit/<id>/', methods=['GET', 'POST'])
def edit(id):
    office=Office.query.get(id)
    if office is None:
        abort(404)

    edit_form = forms.EditForm(request.form,office)
    if request.method == 'POST' and edit_form.validate():
        office.name = edit_form.name.data
        office.address = edit_form.address.data
        office.minister = edit_form.minister.data


        db.session.commit()

        success_message = 'Oficina actualizada en Bd'
        flash(success_message)
    return render_template('edit.html', form=edit_form)
@app.route('/office/delete/<id>/', methods=['GET', 'POST'])
def delete(id):
    office=Office.query.get(id)
    if office is None:
        abort(404)

    delete_form = forms.DeleteForm(request.form,office)
    if request.method == 'POST' and delete_form.validate():
        office.name = delete_form.name.data
        office.address = delete_form.address.data
        office.minister = delete_form.minister.data

        db.session.delete(office)
        db.session.commit()

        success_message = 'Oficina eliminada en Bd'
        flash(success_message)
    return render_template('delete.html', form=delete_form)

@app.route('/office/read/<id>/', methods=['GET'])
def read(id):
    office=Office.query.get(id)
    if office is None:
        abort(404)


    return render_template('read.html',office=office)

@app.route('/user', methods=['GET', 'POST'])
def create_user():
    create_user = forms.CreateUser(request.form)
    if request.method == 'POST' and create_user.validate():
        user = User(username=create_user.username.data,
                    password=create_user.password.data,
                    email=create_user.email.data)

        db.session.add(user)
        db.session.commit()

        success_message = 'Usuario registrado en la BD'
        flash(success_message)

    return render_template('user.html', form=create_user)